package it.addressbook

class Phone {

    String number
    String type

    static constraints = {
        number size: 1..20
        type inList: ['Home', 'Work', 'Mobile']
    }
}

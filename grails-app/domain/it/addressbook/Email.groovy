package it.addressbook

class Email {

    String email

    static constraints = {
        email nullable: false, email: true
    }
}

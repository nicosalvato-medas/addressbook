package it.addressbook

class Contact {

    Date dateCreated
    Date lastUpdated

    String givenName
    String familyName
    String homeAddress

    //TODO validate every email
    static hasMany = [phoneList: Phone, emailList: Email]

    static constraints = {
        givenName size: 1..128
        familyName size: 1..128
        homeAddress nullable: true, size: 0..1024
        emailList nullable: false, minSize: 1
        phoneList nullable: true, minSize: 1
    }
}

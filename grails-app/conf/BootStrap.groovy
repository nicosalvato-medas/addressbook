import it.addressbook.Contact
import it.addressbook.Email
import it.addressbook.Phone

class BootStrap {

    def init = { servletContext ->
        (1..100).each {
            def email = new Email(email: "test$it@medas.it").save(flush: true, failOnError: true)
            def phone = new Phone(type: "Mobile", number: "333333$it").save(flush: true, failOnError: true)
            def contact = new Contact(familyName: "ftest$it", givenName: "gtest$it")
            contact.addToEmailList(email)
            contact.addToPhoneList(phone)
            contact.save(flush: true, failOnError: true)
        }
    }
    def destroy = {
    }
}

<%@ page import="it.addressbook.Phone" %>



<div class="fieldcontain ${hasErrors(bean: phoneInstance, field: 'number', 'error')} required">
	<label for="number">
		<g:message code="phone.number.label" default="Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="number" maxlength="20" required="" value="${phoneInstance?.number}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: phoneInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="phone.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="type" from="${phoneInstance.constraints.type.inList}" required="" value="${phoneInstance?.type}" valueMessagePrefix="phone.type"/>

</div>


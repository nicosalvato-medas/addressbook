<%@ page import="it.addressbook.Contact" %>



<div class="fieldcontain ${hasErrors(bean: contactInstance, field: 'givenName', 'error')} required">
	<label for="givenName">
		<g:message code="contact.givenName.label" default="Given Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="givenName" maxlength="128" required="" value="${contactInstance?.givenName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: contactInstance, field: 'familyName', 'error')} required">
	<label for="familyName">
		<g:message code="contact.familyName.label" default="Family Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="familyName" maxlength="128" required="" value="${contactInstance?.familyName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: contactInstance, field: 'homeAddress', 'error')} ">
	<label for="homeAddress">
		<g:message code="contact.homeAddress.label" default="Home Address" />
		
	</label>
	<g:textArea name="homeAddress" cols="40" rows="5" maxlength="1024" value="${contactInstance?.homeAddress}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: contactInstance, field: 'emailList', 'error')} required">
	<label for="emailList">
		<g:message code="contact.emailList.label" default="Email List" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="emailList" from="${it.addressbook.Email.list()}" multiple="multiple" optionKey="id" size="5" required="" value="${contactInstance?.emailList*.id}" class="many-to-many"/>
	<g:link controller="email" action="create">Add email address</g:link>
</div>

<div class="fieldcontain ${hasErrors(bean: contactInstance, field: 'phoneList', 'error')} ">
	<label for="phoneList">
		<g:message code="contact.phoneList.label" default="Phone List" />
		
	</label>
	<g:select name="phoneList" from="${it.addressbook.Phone.list()}" multiple="multiple" optionKey="id" size="5" value="${contactInstance?.phoneList*.id}" class="many-to-many"/>
	<g:link controller="phone" action="create">Add phone number</g:link>
</div>


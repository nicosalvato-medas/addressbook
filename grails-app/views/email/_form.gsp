<%@ page import="it.addressbook.Email" %>



<div class="fieldcontain ${hasErrors(bean: emailInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="email.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${emailInstance?.email}"/>

</div>


# README #

The AddressBook repository holds an address book app in Grails, for training purpose.

## Table of content ##

[TOC]

## General Info ##

* Grails version: 2.4.4

* Contributors: Nicolò Salvato, Riccardo Gorrini
package it.addressbook

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Phone)
class PhoneSpec extends ConstraintUnitSpec {

    def setup() {
        //mock a phone with some data
        mockForConstraintsTests(Phone, [new Phone()])
    }

    @Unroll("test phone constraint: #field #error")
    def "test phone constraints"() {
        when:
        def obj = new Phone("$field": val)

        then:
        validateConstraints(obj, field, error)

        where:
        error           | field          | val
        'size'          | 'number'       | getLongString(21)
        'nullable'      | 'number'       | null
        'nullable'      | 'number'       | ''
        'nullable'      | 'number'       | ' '
        'valid'         | 'number'       | '02789563'
        'inList'        | 'type'         | 'Beach'
        'nullable'      | 'type'         | null
        'nullable'      | 'type'         | ''
        'nullable'      | 'type'         | ' '
        'valid'         | 'type'         | 'Mobile'
        'valid'         | 'type'         | 'Work'
        'valid'         | 'type'         | 'Home'
    }
}

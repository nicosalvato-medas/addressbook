package it.addressbook

import grails.test.mixin.TestFor
import groovy.mock.interceptor.MockFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Contact)
class ContactSpec extends ConstraintUnitSpec {

    def setup() {
        mockForConstraintsTests(Contact, [new Contact()])
    }

    @Unroll("test contact constraint: #field #error")
    def "test contact constraints"() {
        when:
        def obj = new Contact("$field": val)

        then:
        validateConstraints(obj, field, error)

        where:
        error           | field          | val
        'size'          | 'givenName'    | getLongString(129)
        'nullable'      | 'givenName'    | null
        'nullable'      | 'givenName'    | ''
        'nullable'      | 'givenName'    | ' '
        'valid'         | 'givenName'    | 'Jimi'
        'size'          | 'familyName'   | getLongString(129)
        'nullable'      | 'familyName'   | null
        'nullable'      | 'familyName'   | ''
        'nullable'      | 'familyName'   | ' '
        'valid'         | 'familyName'   | 'Hendrix'
        'size'          | 'homeAddress'  | getLongString(1025)
        'valid'         | 'homeAddress'  | null
        'valid'         | 'homeAddress'  | ''
        'valid'         | 'homeAddress'  | ' '
        'valid'         | 'homeAddress'  | 'Via Privata Benadir 14'
        'minSize'       | 'phoneList'    | createPhone(0)
        'valid'         | 'phoneList'    | createPhone(1)
        'valid'         | 'phoneList'    | createPhone(7)
    }

    private static createPhone(Integer count) {
        def phoneList = []
        count.times {
            phoneList << new Phone(type: 'Mobile', number: '024578963')
        }
        phoneList
    }
}

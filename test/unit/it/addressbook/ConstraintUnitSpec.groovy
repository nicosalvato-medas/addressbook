package it.addressbook

import spock.lang.Specification

/**
 * Created by nicolo.salvato on 09/03/2015.
 */
class ConstraintUnitSpec extends Specification {

    // String generator
    String getLongString(Integer length) {
        'a' * length
    }

    // Email generator
    String getEmail(Boolean valid) {
        valid ? "test@medas.it" : "test-medas.it"
    }

    // Validate constraints
    void validateConstraints(obj, field, error) {
        def validated = obj.validate()
        if (error && error != 'valid') {
            assert !validated
            assert obj.errors[field]
            assert error == obj.errors[field]
        } else {
            assert !obj.errors[field]
        }
    }
}
